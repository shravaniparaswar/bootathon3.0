

# Simultaneous Equation : Gauss Elimination Methord  

## AIM
To solve simultaneous equation by Gauss Elimination Methord

##PROCEDURE

Example:  
0.3x<sub>1</sub> - 0.2x<sub>2</sub> + 10x<sub>3</sub> = 71.4  
 3x<sub>1</sub> - 0.1x<sub>2</sub> - 0.2x<sub>3</sub> = 7.85  
 0.1x<sub>1</sub> + 7x<sub>2</sub> - 0.3x<sub>3</sub> =- 19.3

 Select no of equation 3 and Enter the data in the following format and hit solve.  

 ![image004](images/image004.jpg)


## THEORY    
 * system of linear equation is represented as :  

A<sub>11</sub>X<sub>1</sub> + A<sub>12</sub>X<sub>2</sub> + A<sub>13</sub>X<sub>3</sub> .......A<sub>1n</sub>X<sub>n</sub>  
A<sub>21</sub>X<sub>1</sub> +A<sub>22</sub>X<sub>2</sub> +A<sub>23</sub>X<sub>3</sub>.......A<sub>2n</sub>X<sub>n</sub>    
A<sub>n1</sub>X<sub>1</sub>+A<sub>n2</sub>X<sub>2</sub>+A<sub>n3</sub>X<sub>3</sub> .......A<sub>nn</sub>X<sub>n</sub>      

The above can be represented as:   
[A][X]=[C]  
[A]->Coefficient matrix  
[X]->Variable matrix  
[C]->Constantm matrix  

* Step 1 .perform partial pivoting    

Arrange the system of equation in such a way that digonl elements will have maximum absolute value.  

* step a: find maximum absolute value of coefficient of X <sub>1</sub> from all equation and place that equation in first row.

* step b : finf maximum absolute value of coefficient X<sub>2</sub> from rest of the equation and place that equation in second row.

* step n-1: find maximum absolute value of coefficient of X<sub>n-1</sub>from rest of the equation and place the equation in (n-1)<sup>th</sup>row.
 
 Now the system after partial pivoting as :  
 P<sub>11</sub>X<sub>1</sub>+P<sub>12</sub>X<sub>2</sub>+P<sub>13</sub>X<sub>3</sub>+........P<sub>1n</sub>X<sub>n</sub>=PC<sub>1</sub> 
  P<sub>21</sub>X<sub>1</sub>+P<sub>22</sub>X<sub>2</sub>+P<sub>23</sub>X<sub>3</sub>+........P<sub>2n</sub>X<sub>n</sub>=PC<sub>2</sub>
   P<sub>n1</sub>X<sub>1</sub>+P<sub>n2</sub>X<sub>2</sub> +P<sub>n3</sub>X<sub>3</sub>+........P<sub>nn</sub>X<sub>n</sub>=PC<sub>n</sub>  


 Step 2: reduce the system of equation to upper triangular form  
 
 * step a: using first row make all elements in the first column below the first row to zero . R<sub>k</sub>=R<sub>k-m<sub>k</sub></sub>*R<sub>1</sub> where m<sub>k</sub>=P<sub>k</sub>/P<sub>11</sub> where k=2 to n    
 * step b :using second row make all elements in second column below second row zero.R<sub>k</sub>=R<sub>k-m<sub>k</sub></sub>*R<sub>2</sub> where m<sub>k</sub>=P<sub>k2</sub>/P<sub>22</sub> .  

 * step n-1:using (n-1)<sup>th</sup> row make all elements in (n-1)<sup>th</sup> column below (n-1)<sup>th</sup>row zero. R<sub>k</sub>=R<sub>k-m<sub>k</sub></sub>*R<sub>n-1</sub> where m<sub>k</sub>=P<sub>k2</sub>/P<sub>n-1</sub> where k= n to n .  
 
 Now system after reducing to upper traingular matrix  



  * step 2 :Back substitution methord    
 1. step a :from last row X<sub>n</sub>=UC<sub>n</sub>/U 

 2. Step b: from second last row 
 ![image001](images/image001.png)

 3. Step n: So in general   
 ![image002](images/image002.png)

 Result :
 Example:

1.48x1 + 0.93x2 ‐ 1.3x3 = 1.03  
2.51x1 + 1.48x2 + 4.53x3 = 0.05  
3.04x1 + 2.68x2 ‐ 1.48x3 = ‐0.53  

Solution:  
All the figures are rounded off to two digits after decimal point.

![image003](images/image003.png)

After partial pivoting :  

![image004](images/image004.png)

Reducing to upper triangular form  

![image005](images/image005.png)

By back substitution

x<sub>1</sub>= 2.312,   x<sub>2</sub>=‐2.986, x<sub>3</sub>=‐0.296
